package model.vo;

/**
 * Representation of a Service object
 */

public class Servicio implements Comparable<Servicio>
{	
	private String taxiId;
	private String tripId;
	private String tripSeconds;
	private String tripMiles;
	private String tripTotal;
	private RangoFechaHora fecha;
	private String zonaInicio;
	private String zonaFinal;
	private String compania;
	
	
	public Servicio(String compania,String tripId, String taxiId, String seconds, String miles, String total,String fechaInicial, String horaInicial, String fechaFinal, String horaFinal, String zonaI, String zonaF)
	{
		
		this.tripId=tripId;
		this.taxiId=taxiId;
		tripSeconds=seconds;
		tripMiles=miles;
		tripTotal=total;
		fecha= new RangoFechaHora(fechaInicial,fechaFinal,horaInicial,horaFinal);
		zonaInicio=zonaI;
		zonaFinal=zonaF;
		this.compania=compania;
		
	}
	
	public String getCompania() {
		return compania;
	}


	public void setCompania(String compania) {
		this.compania = compania;
	}
	
	public String getZonaInicio() {
		return zonaInicio;
	}


	public void setZonaInicio(String zonaInicio) {
		this.zonaInicio = zonaInicio;
	}


	public String getZonaFinal() {
		return zonaFinal;
	}


	public void setZonaFinal(String zonaFinal) {
		this.zonaFinal = zonaFinal;
	}

	
	
	public RangoFechaHora getFecha() {
		return fecha;
	}

	public boolean estaEnRango(RangoFechaHora rango)
	{
		return fecha.estaEnRango(rango);
	}

	public boolean iniciaEnRango(RangoFechaHora rango)
	{
		return fecha.iniciaEnRango(rango);
	}
	public boolean terminaEnRango(RangoFechaHora rango)
	{
		return fecha.terminaEnRango(rango);
	}

	public void setFecha(RangoFechaHora fecha) {
		this.fecha = fecha;
	}



	public void setTripId(String tripId) {
		this.tripId = tripId;
	}



	public void setTaxiId(String taxiId) {
		this.taxiId = taxiId;
	}



	public void setTripSeconds(String tripSeconds) {
		this.tripSeconds = tripSeconds;
	}



	public void setTripMiles(String tripMiles) {
		this.tripMiles = tripMiles;
	}



	public void setTripTotal(String tripTotal) {
		this.tripTotal = tripTotal;
	}

	public String getTripId() {
		return tripId;
	}



	public String getTaxiId() {
		return taxiId;
	}



	public String getTripSeconds() {
		return tripSeconds;
	}



	public String getTripMiles() {
		return tripMiles;
	}



	public String getTripTotal() {
		return tripTotal;
	}

	
	

	@Override
	public int compareTo(Servicio nServicio) {
		
		if(fecha.getFechaInicial().compareTo(nServicio.getFecha().getFechaInicial())==0)
			return fecha.getHoraInicio().compareTo(nServicio.getFecha().getHoraInicio());
		return fecha.getFechaInicial().compareTo(nServicio.getFecha().getFechaInicial());	}
}
