package model.vo;

import java.util.Iterator;

import model.data_structures.ILinkedList;
import model.data_structures.ListaDoblementeEncadenada;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>
{

	private String taxiId;
	private String compania;
	private ListaDoblementeEncadenada<Servicio> serviciosTaxi ;
	private double dineroTotal=0;
	private double millasTotales=0;

	public Taxi(String id, String compania,Servicio serv)
	{
		taxiId=id;
		this.compania=compania;
		dineroTotal+=Double.parseDouble(serv.getTripTotal());
		millasTotales+=Double.parseDouble(serv.getTripMiles());
		serviciosTaxi=new ListaDoblementeEncadenada<Servicio>(serv);
	}
	public Taxi(String id, String compania)
	{
		taxiId=id;
		this.compania=compania;
		serviciosTaxi=new ListaDoblementeEncadenada<Servicio>();
	}

	public void addSevice(Servicio serv)
	{
		dineroTotal+=Double.parseDouble(serv.getTripTotal());
		millasTotales+=Double.parseDouble(serv.getTripMiles());
		serviciosTaxi.add(serv);
	}

	public ListaDoblementeEncadenada<Servicio> getServiciosTaxi() {
		return serviciosTaxi;
	}


	/**
	 * @return id - taxi_id
	 */
	public String getTaxiId()
	{
		// TODO Auto-generated method stub
		return taxiId;
	}

	/**
	 * @return company
	 */
	public String getCompany()
	{
		// TODO Auto-generated method stub
		return compania;
	}

	@Override
	public int compareTo(Taxi o) 
	{
		// TODO Auto-generated method stub
		return compania.compareTo(o.getCompany());
	}	

	public double darGananciaEnRango(RangoFechaHora rango)
	{
		Iterator<Servicio> iter=serviciosTaxi.iterator();
		double ganancia=0.0;
		while(iter.hasNext())
		{
			Servicio actual=iter.next();
			if(actual.iniciaEnRango(rango))
			{
				if(actual.terminaEnRango(rango))
					ganancia+=Double.parseDouble(actual.getTripTotal());
				while(iter.hasNext()&&(actual=iter.next()).iniciaEnRango(rango))
				{
					if(actual.terminaEnRango(rango))
						ganancia+=Double.parseDouble(actual.getTripTotal());							
				}
				return ganancia;
			}
		}
		return ganancia;


	}
	public int darServEnRango(RangoFechaHora rango)
	{
		Iterator<Servicio> iter=serviciosTaxi.iterator();
		int servicios=0;
		while(iter.hasNext())
		{
			Servicio actual=iter.next();
			if(actual.getFecha().compareTo(rango)==1)
			{
				servicios++;

			}
			else if(actual.getFecha().compareTo(rango)==-1)
			{
				break;
			}
		}
		return servicios;


	}

	public InfoTaxiRango darInformacionTaxiEnRango(RangoFechaHora rango)
	{
		Iterator<Servicio> iter=serviciosTaxi.iterator();
		ILinkedList<Servicio>servicios=new ListaDoblementeEncadenada<Servicio>();
		InfoTaxiRango info= new InfoTaxiRango();
		double plata=0.0;
		double distancia=0.0;
		double tiempo=0.0;

		while(iter.hasNext())
		{
			Servicio actual=iter.next();
			if(actual.getFecha().compareTo(rango)==1)
			{
				servicios.add(actual);
				plata+=Double.parseDouble(actual.getTripTotal());
				tiempo+=Double.parseDouble(actual.getTripSeconds());
				distancia+=Double.parseDouble(actual.getTripMiles());

			}
			else if(actual.getFecha().compareTo(rango)==-1)
			{
				break;
			}
		}
		info.setCompany(compania);
		info.setIdTaxi(taxiId);
		info.setRango(rango);
		info.setDistanciaTotalRecorrida(distancia);
		info.setPlataGanada(plata);
		info.setServiciosPrestadosEnRango(servicios);
		info.setTiempoTotal(tiempo+"");

		return info;
	}
	public double getMillas() {
		return millasTotales;
	}

	public double getDinero() {
		return dineroTotal;
	}



}