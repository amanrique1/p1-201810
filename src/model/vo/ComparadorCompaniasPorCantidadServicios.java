package model.vo;

import java.util.Comparator;

public class ComparadorCompaniasPorCantidadServicios implements Comparator<Compania> {

	@Override
	public int compare(Compania comp1, Compania comp2) {
		if(comp1.getServiciosAsociados().size()>comp2.getServiciosAsociados().size())
			return -1;
		if(comp1.getServiciosAsociados().size()<comp2.getServiciosAsociados().size())
			return 1;
		return 0;
	}

}
