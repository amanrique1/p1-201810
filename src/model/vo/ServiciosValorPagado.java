package model.vo;

import model.data_structures.ListaDoblementeEncadenada;

public class ServiciosValorPagado {
	
	private ListaDoblementeEncadenada<Servicio> serviciosAsociados;
	private double valorAcumulado;
	
	public ServiciosValorPagado()
	{
		serviciosAsociados=new ListaDoblementeEncadenada<Servicio>();
		valorAcumulado=0.0;
	}
	
	public void agregarServicio(Servicio servicio) {
		serviciosAsociados.add(servicio);
		valorAcumulado+=Double.parseDouble(servicio.getTripTotal());
	}
	public ListaDoblementeEncadenada<Servicio> getServiciosAsociados() {
		return serviciosAsociados;
	}
	public void setServiciosAsociados(ListaDoblementeEncadenada<Servicio> serviciosAsociados) {
		this.serviciosAsociados = serviciosAsociados;
	}
	public double getValorAcumulado() {
		return valorAcumulado;
	}
	public void setValorAcumulado(double valorAcumulado) {
		this.valorAcumulado = valorAcumulado;
	}
	
	

}
