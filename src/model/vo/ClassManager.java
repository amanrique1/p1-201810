package model.vo;

import java.util.Comparator;
import java.util.Iterator;

import api.ITaxiTripsManager;
import model.data_structures.ILinkedList;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.LinkedListQueue;
import model.data_structures.LinkedListStack;
import model.data_structures.ListaDoblementeEncadenada;

public class ClassManager implements ITaxiTripsManager {

	private ListaDoblementeEncadenada<Servicio> servicios;
	private ListaDoblementeEncadenada<Servicio> serviciosAux;
	private ListaDoblementeEncadenada<Taxi> taxis;
	private ListaDoblementeEncadenada<Compania> companias;
	private ListaDoblementeEncadenada<ZonaServicios> zonas;
	

	public ClassManager()
	{
		servicios=new ListaDoblementeEncadenada<Servicio>();
		taxis=new ListaDoblementeEncadenada<Taxi>();
		companias=new ListaDoblementeEncadenada<Compania>();
		serviciosAux=new ListaDoblementeEncadenada<Servicio>();
		zonas=new ListaDoblementeEncadenada<ZonaServicios>();


	}
	public ListaDoblementeEncadenada<Servicio> getServicios() {
		return servicios;
	}
	public void setServicios(ListaDoblementeEncadenada<Servicio> servicios) {
		this.servicios = servicios;
	}
	public ListaDoblementeEncadenada<Servicio> getServiciosAux() {
		return serviciosAux;
	}
	public void setServiciosAux(ListaDoblementeEncadenada<Servicio> serviciosAux) {
		this.serviciosAux = serviciosAux;
	}
	public ListaDoblementeEncadenada<Taxi> getTaxis() {
		return taxis;
	}
	public void setTaxis(ListaDoblementeEncadenada<Taxi> taxis) {
		this.taxis = taxis;
	}
	public ListaDoblementeEncadenada<Compania> getCompanias() {
		return companias;
	}
	public void setCompanias(ListaDoblementeEncadenada<Compania> companias) {
		this.companias = companias;
	}

	public void agregarTaxiServicioACompania(String nombre, Taxi taxi, Servicio servicio)
	{


		boolean esta=false;
		Iterator<Compania> iter=companias.iterator();
		while(iter.hasNext()&&!esta)
		{
			Compania actual=iter.next();
			if(actual.getNombre().equalsIgnoreCase(nombre))
			{
				esta=true;
				actual.addTaxi(taxi);
				actual.addServicio(servicio);
			}
		}


		if(!esta)
			companias.addInOrder(new Compania(nombre,taxi,servicio));
		

	}
	public Taxi agregarTaxi(String id, String compania, Servicio serv)
	{

		boolean esta=false;
		Iterator<Taxi> iter=taxis.iterator();
		while(iter.hasNext()&&!esta)
		{
			Taxi actual=iter.next();
			if(actual.getTaxiId().equalsIgnoreCase(id))
			{
				esta=true;
				actual.addSevice(serv);
			}
		}


		if(!esta)
		{
			Taxi taxi=new Taxi(id,compania,serv);
			taxis.add(taxi);
			return taxi;
		}
		return null;

	}
	public void agregarZona(String zona,Servicio servicio) {
		boolean esta=false;
		Iterator<ZonaServicios> iter=zonas.iterator();
		while(iter.hasNext()&&!esta)
		{
			ZonaServicios actual=iter.next();
			if(actual.getIdZona().equals(zona))
			{
				esta=true;
				actual.addFechaServicios(servicio);
			}
		}


		if(!esta)
			zonas.addInOrder(new ZonaServicios(zona, servicio));
	
	}
	
	
	
	public void agregarServicio(int estado,String tripId, String taxiId, String seconds, String miles, String total,String fechaInicial, String horaInicial, String fechaFinal, String horaFinal, String compania,String zonaInicio, String zonaFinal)
	{
		if(estado==0)
		{
			servicios.enlazar(serviciosAux);
			serviciosAux.limpiar();
			return;
		}

		if(estado==1)
		{
			servicios.addInOrder(new Servicio(compania,tripId, taxiId, seconds,miles, total, fechaInicial, horaInicial,fechaFinal,horaFinal,zonaInicio,zonaFinal));

		}
		else if(estado>1)
		{
			serviciosAux.addInOrder(new Servicio(compania,tripId, taxiId, seconds,miles, total, fechaInicial, horaInicial,fechaFinal,horaFinal,zonaInicio,zonaFinal));

		}
	}

	public void agregarCompaniasTaxisZonas()
	{
		Iterator<Servicio> iter= servicios.iterator();
		while(iter.hasNext())
		{
			Servicio actual=iter.next();
			if(!actual.getZonaInicio().equals("-1"))
				agregarZona(actual.getZonaInicio(), actual);
			Taxi taxi= agregarTaxi(actual.getTaxiId(), actual.getCompania(), actual);
			if(taxi!=null&&!actual.getCompania().equals("Independiente"))	
				agregarTaxiServicioACompania(actual.getCompania(), taxi, actual);
			
		}	
	}



	@Override //1A
	public IQueue <Servicio> darServiciosEnPeriodo(RangoFechaHora rango)
	{
		IQueue<Servicio> colaEnPeriodo= new LinkedListQueue<Servicio>();
		Iterator<Servicio> iter=servicios.iterator();
		while(iter.hasNext())
		{
			Servicio actual=iter.next();
			if(actual.getFecha().compareTo(rango)==1)
			{
				colaEnPeriodo.enqueue(actual);

			}
			else if(actual.getFecha().compareTo(rango)==-1)
			{
				break;
			}

		}	

		return colaEnPeriodo;
	}

	@Override //2A
	public Taxi darTaxiConMasServiciosEnCompaniaYRango(RangoFechaHora rango, String company) throws Exception
	{
		Iterator<Compania> iterComp=companias.iterator();
		while(iterComp.hasNext())
		{
			Compania comp=iterComp.next();
			if(company.equalsIgnoreCase(comp.getNombre()))
			{
				return comp.darTaxiConMasServEnRango(rango);

			}
		}
		if(true)
			throw new Exception("la compa�ia "+company+" no existe en la lista");
		return null;
	}

	@Override //3A
	public InfoTaxiRango darInformacionTaxiEnRango(String id, RangoFechaHora rango) throws Exception
	{
		// TODO Auto-generated method stub
		Iterator<Taxi> iterComp=taxis.iterator();
		while(iterComp.hasNext())
		{
			Taxi comp=iterComp.next();
			if(id.equals(comp.getTaxiId()))
			{
				return comp.darInformacionTaxiEnRango(rango);

			}
		}
		if(true)
			throw new Exception("el taxi con id: "+id+" no existe en la lista");
		return null;
	}

	@Override //4A
	public ListaDoblementeEncadenada<RangoDistancia> darListaRangosDistancia(String fecha, String horaInicial, String horaFinal) 
	{
		Iterator<Servicio> iterComp=servicios.iterator();
		RangoFechaHora rango= new RangoFechaHora(fecha, fecha, horaInicial, horaFinal);
		ListaDoblementeEncadenada<RangoDistancia> lista= new ListaDoblementeEncadenada<RangoDistancia>();
		RangoDistancia actual2= new RangoDistancia();
		while(iterComp.hasNext())
		{
			Servicio actual=iterComp.next();
			if(actual.getFecha().compareTo(rango)==1)
			{

				int limiteInferior= (int)Double.parseDouble(actual.getTripMiles());
				actual2.setLimineInferior(limiteInferior);
				actual2.setLimiteSuperior(limiteInferior+1);
				RangoDistancia rg=null;
				Iterator<RangoDistancia>iter2=lista.iterator();
				while(iter2.hasNext())
				{
					
					RangoDistancia rgactual=iter2.next();
					System.out.println(rgactual.getLimineInferior()+"     "+limiteInferior);
					if(rgactual.getLimineInferior()==limiteInferior)
					{
						rg=rgactual;
						break;
					}
				}
						
				

				if(rg==null)
				{
					actual2.setServiciosEnRango(actual);
					lista.addInOrder(actual2);

				}
				else
				{
					rg.setServiciosEnRango(actual);
				}

			}
			else if(actual.getFecha().compareTo(rango)==-1)
			{
				break;
			}
		}

		return lista;
	}

	@Override //1B
	public ListaDoblementeEncadenada<Compania> darCompaniasTaxisInscritos() 
	{
		// TODO Auto-generated method stub


		return companias;
	}

	@Override //2B
	public Taxi darTaxiMayorFacturacion(RangoFechaHora rango, String nomCompania) throws Exception
	{
		Iterator<Compania> iterComp=companias.iterator();
		while(iterComp.hasNext())
		{
			Compania comp=iterComp.next();
			if(nomCompania.equals(comp.getNombre()))
			{
				return comp.darTaxiMayorFacturacion(rango);

			}
		}
		if(true)
			throw new Exception("la compa�ia "+nomCompania+" no esxiste en la lista");
		return null;
	}

	@Override //3B
	public ServiciosValorPagado[] darServiciosZonaValorTotal(RangoFechaHora rango, String idZona)
	{
		Iterator<Servicio> iter=servicios.iterator();
		ServiciosValorPagado empieza=new ServiciosValorPagado();
		ServiciosValorPagado termina=new ServiciosValorPagado();
		ServiciosValorPagado empiezaYTermina=new ServiciosValorPagado();

		boolean ya=false;
		while(iter.hasNext()&&!ya)
		{
			Servicio actual= iter.next();
			if(actual.iniciaEnRango(rango))
			{
				if(actual.terminaEnRango(rango))
				{					
					if(actual.getZonaInicio().equals(idZona)&&actual.getZonaFinal().equals(idZona))
						empiezaYTermina.agregarServicio(actual);
					else if(actual.getZonaInicio().equals(idZona))
						empieza.agregarServicio(actual);
					else if(actual.getZonaFinal().equals(idZona))
						termina.agregarServicio(actual);
				}
				while(iter.hasNext()&&(actual=iter.next()).iniciaEnRango(rango))
				{

					if(actual.terminaEnRango(rango))
					{					
						if(actual.getZonaInicio().equals(idZona)&&actual.getZonaFinal().equals(idZona))
							empiezaYTermina.agregarServicio(actual);
						else if(actual.getZonaInicio().equals(idZona))
							empieza.agregarServicio(actual);
						else if(actual.getZonaFinal().equals(idZona))
							termina.agregarServicio(actual);
					}
				}
				ya=true;
				
			}
			
		}
		ServiciosValorPagado[] serviciosValorPagado=new ServiciosValorPagado[3];
		serviciosValorPagado[0]=empieza;
		serviciosValorPagado[1]=termina;
		serviciosValorPagado[2]=empiezaYTermina;
		return serviciosValorPagado;

		
	}

	@Override //4B
	public ListaDoblementeEncadenada<ZonaServicios> darZonasServicios(RangoFechaHora rango)
	{
		ListaDoblementeEncadenada<ZonaServicios>zonasRango=zonas;
		Iterator<ZonaServicios> iter=zonasRango.iterator();
		while(iter.hasNext())
			iter.next().acomodarARango(rango);
		return zonasRango;
	}

	@Override //2C
	public ListaDoblementeEncadenada<Compania> companiasMasServicios(RangoFechaHora rango, int n) throws Exception
	{
		
		if(n>companias.size())
			throw new Exception("Solo hay "+companias.size()+ "Compa�ias");
	
		ListaDoblementeEncadenada<Compania> lista=companias;
		ListaDoblementeEncadenada<Compania> companiasMasServicios=new ListaDoblementeEncadenada<Compania>();
		Comparator<Compania> comparadorCantServ=new ComparadorCompaniasPorCantidadServicios();
		Iterator<Compania> iter=lista.iterator();
		while(iter.hasNext())
		{
			Compania actual=iter.next();
			actual.acomodarServiciosARango(rango);
			companiasMasServicios.addInOrderComparador(comparadorCantServ, actual);
		}
		
		return companiasMasServicios.subList(0, n-1);
	
	
	}

	@Override //3C
	public ListaDoblementeEncadenada<CompaniaTaxi> taxisMasRentables()
	{
		ListaDoblementeEncadenada<CompaniaTaxi> lista=new ListaDoblementeEncadenada<CompaniaTaxi>();
		Iterator<Compania> iterComp=companias.iterator();
		
		while(iterComp.hasNext())
		{
			Compania comp=iterComp.next();
			Taxi mas=comp.darTaxiMasEfectivo();
			if(mas!=null)
			{
			CompaniaTaxi ct=new CompaniaTaxi();
			ct.setNomCompania(comp.getNombre());
			ct.setTaxi(mas);
			lista.add(ct);
			}
		}
		
		return lista;
	}

	@Override //4C
	public IStack <Servicio> darServicioResumen(String taxiId, String horaInicial, String horaFinal, String fecha) 
	{
		IStack<Servicio> pilaServicios=new LinkedListStack<Servicio>();
		ListaDoblementeEncadenada<Servicio>listaFinal=new ListaDoblementeEncadenada<Servicio>();
		RangoFechaHora rango=new RangoFechaHora(fecha, fecha, horaInicial, horaFinal);
		Taxi buscado=null;
		Iterator<Taxi> iter=taxis.iterator();
		double millas=0;
		int segundos=0;
		double total=0;
		while(iter.hasNext())
		{
			Taxi actual=iter.next();
			if(actual.getTaxiId().equalsIgnoreCase(taxiId))
			{
				buscado=actual;
				break;

			}

		}
		if(buscado!=null)
		{
			Iterator<Servicio> iter2=buscado.getServiciosTaxi().iterator();
			while(iter2.hasNext())
			{
				Servicio actual=iter2.next();
				if(actual.getFecha().compareTo(rango)==1)
				{
					millas+=Double.parseDouble(actual.getTripMiles());
					segundos+=Integer.parseInt(actual.getTripSeconds());
					total+=Double.parseDouble(actual.getTripTotal());
					if(millas<=10)
					{
						pilaServicios.push(actual);
					}
					else
					{
						millas-=Double.parseDouble(actual.getTripMiles());
						Servicio pServicio= new Servicio(actual.getCompania(), "Servicios de:"+taxiId, taxiId, segundos+"", millas+"", total+"", fecha, horaInicial, fecha, horaFinal, "", "");
						pilaServicios.vaciar();
						listaFinal.addInOrder(pServicio);
						millas=0;
						segundos=0;
						total=0;
					}

				}
				else if(actual.getFecha().compareTo(rango)==-1)
				{
					break;
				}

			}
			
			while (pilaServicios.size()>0) 
			{
				
				listaFinal.add(pilaServicios.pop());
			}
			Iterator<Servicio> iter3=listaFinal.iterator();
			while (iter3.hasNext()) 
			{
				Servicio actual=iter3.next();
				pilaServicios.push(actual); 
			}

		}
		return pilaServicios;
	}




	@Override
	public boolean cargarSistema(String direccionJson) {
		// NO SE USA EN ESTA CLASE
		return false;
	}





}
