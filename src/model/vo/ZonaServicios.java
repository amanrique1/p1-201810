package model.vo;

import java.util.Iterator;

import model.data_structures.ListaDoblementeEncadenada;

public class ZonaServicios implements Comparable<ZonaServicios>{

	private String idZona;
	
	private ListaDoblementeEncadenada<FechaServicios> fechasServicios;
	
	public ZonaServicios(String id, Servicio servicio)
	{
		idZona=id;
		fechasServicios=new ListaDoblementeEncadenada<FechaServicios>(new FechaServicios(servicio));
	}

	public String getIdZona() {
		return idZona;
	}
	
	public void addFechaServicios(Servicio servicio)
	{
		Iterator<FechaServicios> iter=fechasServicios.iterator();
		boolean esta=false;
		while(iter.hasNext()&&!esta)
		{
			FechaServicios actual=iter.next();
			if(actual.getFecha().equals(servicio.getFecha().getFechaInicial()))
			{
				actual.addServicio(servicio);
				esta=true;
			}
		}
		if(!esta)
			fechasServicios.add(new FechaServicios(servicio));
		
	}
	
	public void acomodarARango(RangoFechaHora rango)
	{
		Iterator<FechaServicios> iter=fechasServicios.iterator();
		String inicio= rango.getFechaInicial();
		String fin= rango.getFechaFinal();
		int i=-1;
		int f=-1;
		while(iter.hasNext())
		{
			
			i++;
			FechaServicios actual=iter.next();
			if(actual.getFecha().compareTo(inicio)>=0&&actual.getFecha().compareTo(fin)<=0)
			{
				f=i;
				if(actual.getFecha().compareTo(inicio)==0)
					actual.acomodarARango(rango,0);
				if(actual.getFecha().compareTo(fin)==0)
					actual.acomodarARango(rango,1);
				else
				{
					while(iter.hasNext()&&(actual=iter.next()).getFecha().compareTo(fin)<=0)
					{
						if(actual.getFecha().compareTo(fin)==0)
							actual.acomodarARango(rango,1);
						f++;
					}
				}
				fechasServicios=fechasServicios.subList(i, f);
				return;
			}
			
			
		}	
		fechasServicios.limpiar();
	}


	public void setIdZona(String idZona) {
		this.idZona = idZona;
	}



	public ListaDoblementeEncadenada<FechaServicios> getFechasServicios() {
		return fechasServicios;
	}



	public void setFechasServicios(ListaDoblementeEncadenada<FechaServicios> fechasServicios) {
		this.fechasServicios = fechasServicios;
	}



	@Override
	public int compareTo(ZonaServicios o) {
		// TODO Auto-generated method stub
		int zona1= Integer.parseInt(idZona);
		int zona2= Integer.parseInt(o.getIdZona());
		if(zona1>zona2)
			return 1;
		if(zona1<zona2)
			return -1;
		return 0;

	}
}
