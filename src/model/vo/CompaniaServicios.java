package model.vo;

import model.data_structures.ListaDoblementeEncadenada;

public class CompaniaServicios implements Comparable<CompaniaServicios> {
	
	private String nomCompania;
	
	private ListaDoblementeEncadenada<Servicio> servicios;

	public String getNomCompania() {
		return nomCompania;
	}

	public void setNomCompania(String nomCompania) {
		this.nomCompania = nomCompania;
	}

	public ListaDoblementeEncadenada<Servicio> getServicios() {
		return servicios;
	}

	public void setServicios(ListaDoblementeEncadenada<Servicio> servicios) {
		this.servicios = servicios;
	}

	@Override
	public int compareTo(CompaniaServicios o) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	

}
