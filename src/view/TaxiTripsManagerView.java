package view;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.Scanner;

import controller.Controller;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.LinkedListQueue;
import model.data_structures.ListaDoblementeEncadenada;
import model.data_structures.ILinkedList;
import model.logic.TaxiTripsManager;
import model.vo.Compania;
import model.vo.CompaniaServicios;
import model.vo.CompaniaTaxi;
import model.vo.FechaServicios;
import model.vo.InfoTaxiRango;
import model.vo.RangoDistancia;
import model.vo.RangoFechaHora;
import model.vo.Servicio;
import model.vo.ServicioResumen;
import model.vo.ServiciosValorPagado;
import model.vo.Taxi;
import model.vo.ZonaServicios;

/**
 * view del programa
 */
public class TaxiTripsManagerView 
{

	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		while(!fin)
		{
			//imprime menu
			printMenu();

			//opcion req
			int option = sc.nextInt();

			switch(option)
			{
			//1C cargar informacion dada
			case 1:

				//imprime menu cargar
				printMenuCargar();

				//opcion cargar
				int optionCargar = sc.nextInt();

				//directorio json
				String linkJson = "";
				switch (optionCargar)
				{
				//direccion json pequeno
				case 1:

					linkJson = TaxiTripsManager.DIRECCION_SMALL_JSON;
					break;

					//direccion json mediano
				case 2:

					linkJson = TaxiTripsManager.DIRECCION_MEDIUM_JSON;
					break;

					//direccion json grande
				case 3:

					linkJson = TaxiTripsManager.DIRECCION_LARGE_JSON;
					break;
				}

				//Memoria y tiempo
				long memoryBeforeCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				long startTime = System.nanoTime();

				//Cargar data
				Controller.cargarSistema(linkJson);

				//Tiempo en cargar
				long endTime = System.nanoTime();
				long duration = (endTime - startTime)/(1000000);

				//Memoria usada
				long memoryAfterCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				System.out.println("Tiempo en cargar: " + duration + " milisegundos \nMemoria utilizada:  "+ ((memoryAfterCase1 - memoryBeforeCase1)/1000000.0) + " MB");

				break;

				//1A	
			case 2:

				//fecha inicial
				System.out.println("Ingrese la fecha inicial (Ej : 2017-02-01)");
				String fechaInicialReq1A = sc.next();

				//hora inicial
				System.out.println("Ingrese la hora inicial (Ej: 09:00:00.000)");
				String horaInicialReq1A = sc.next();

				//fecha final
				System.out.println("Ingrese la fecha final (Ej : 2017-02-01)");
				String fechaFinalReq1A = sc.next();

				//hora final
				System.out.println("Ingrese la hora final (Ej: 09:00:00.000)");
				String horaFinalReq1A = sc.next();

				//VORangoFechaHora
				RangoFechaHora rangoReq1A = new RangoFechaHora(fechaInicialReq1A, fechaFinalReq1A, horaInicialReq1A, horaFinalReq1A);

				//Se obtiene la queue dada el rango
				IQueue<Servicio> colaReq1A = Controller.darServiciosEnRango(rangoReq1A);
				//TODO 
				//Recorra la cola y muestre cada servicio en ella
				for (int i = 0; i < colaReq1A.size(); i++) 
				{
					Servicio actual=colaReq1A.dequeue();
					System.out.println("El id del servicio es: "+actual.getTripId()+ " con hora inicial: "+actual.getFecha().getHoraInicio()+" y hora final: "+actual.getFecha().getHoraFinal()+" entre las fechas: "+actual.getFecha().getFechaInicial()+" - "+actual.getFecha().getFechaFinal());
				}
				break;

			case 3: //2A
				try
				{
					//comany
					System.out.println("Ingrese el nombre de la compa��a");
					String companyReq2A =sc.next()+" "+(sc.nextLine()).trim();

					//fecha inicial
					System.out.println("Ingrese la fecha inicial (Ej : 2017-02-01)");
					String fechaInicialReq2A = sc.next();

					//hora inicial
					System.out.println("Ingrese la hora inicial (Ej: 09:00:00.000)");
					String horaInicialReq2A = sc.next();

					//fecha final
					System.out.println("Ingrese la fecha final (Ej : 2017-02-01)");
					String fechaFinalReq2A = sc.next();

					//hora final
					System.out.println("Ingrese la hora final (Ej: 09:00:00.000)");
					String horaFinalReq2A = sc.next();

					//VORangoFechaHora
					RangoFechaHora rangoReq2A = new RangoFechaHora(fechaInicialReq2A, fechaFinalReq2A, horaInicialReq2A, horaFinalReq2A);
					Taxi taxiReq2A = Controller.darTaxiConMasServiciosEnCompaniaYRango(rangoReq2A, companyReq2A);

					//TODO
					//Muestre la info del taxi
					if(taxiReq2A==null)
						System.out.println("No hay ningun taxi de la compa�ia "+companyReq2A+" que haya genenerado servicios en el rango de tiempo dado");
					else
						System.out.println("El taxi que hizo mas servicios en el rango de tiempo fue: "+taxiReq2A.getTaxiId());

				}catch (Exception e) {
					System.out.println(e.getMessage());
				}
				break;

			case 4: //3A
				try
				{
					//comany
					System.out.println("Ingrese el id del taxi");
					String idTaxiReq3A = sc.next();

					//fecha inicial
					System.out.println("Ingrese la fecha inicial (Ej : 2017-02-01)");
					String fechaInicialReq3A = sc.next();

					//hora inicial
					System.out.println("Ingrese la hora inicial (Ej: 09:00:00.000)");
					String horaInicialReq3A = sc.next();

					//fecha final
					System.out.println("Ingrese la fecha final (Ej : 2017-02-01)");
					String fechaFinalReq3A = sc.next();

					//hora final
					System.out.println("Ingrese la hora final (Ej: 09:00:00.000)");
					String horaFinalReq3A = sc.next();

					//VORangoFechaHora
					RangoFechaHora rangoReq3A = new RangoFechaHora(fechaInicialReq3A, fechaFinalReq3A, horaInicialReq3A, horaFinalReq3A);
					InfoTaxiRango taxiReq3A = Controller.darInformacionTaxiEnRango(idTaxiReq3A, rangoReq3A);

					//TODO
					//Muestre la info del taxi
					if(taxiReq3A==null)
						System.out.println("No hay ningun taxi en el "+rangoReq3A+" que haya genenerado servicios en el rango de tiempo dado");
					else
					{
						System.out.println("El taxi con id "+idTaxiReq3A+" que pertenece a: "+taxiReq3A.getCompany()+" recorrio (en millas) "+taxiReq3A.getDistanciaTotalRecorrida()+" en un tiempo de: "+taxiReq3A.getTiempoTotal()+" generando as�: "+taxiReq3A.getPlataGanada()+"\nCon los servicios con:");
						Iterator<Servicio> iter=taxiReq3A.getServiciosPrestadosEnRango().iterator();
						while(iter.hasNext())
						{
							Servicio actual=iter.next();
							System.out.println("ID: "+actual.getTripId()+ " con hora inicial: "+actual.getFecha().getHoraInicio()+" y hora final: "+actual.getFecha().getHoraFinal()+" entre las fechas: "+actual.getFecha().getFechaInicial()+" - "+actual.getFecha().getFechaFinal());


						}	
					}
				}catch (Exception e) {
					System.out.println(e.getMessage());
				}
				break;

			case 5: //4A

				//fecha 
				System.out.println("Ingrese la fecha inicial (Ej : 2017-02-01)");
				String fechaReq4A = sc.next();

				//hora inicial
				System.out.println("Ingrese la hora inicial (Ej: 09:00:00.000)");
				String horaInicialReq4A = sc.next();

				//hora final
				System.out.println("Ingrese la hora final (Ej: 09:00:00.000)");
				String horaFinalReq4A = sc.next();

				ILinkedList<RangoDistancia> listaReq4A = Controller.darListaRangosDistancia(fechaReq4A, horaInicialReq4A, horaFinalReq4A);

				//TODO
				//Recorra la lista y por cada VORangoDistancia muestre los servicios
				Iterator<RangoDistancia> iter=listaReq4A.iterator();
				while(iter.hasNext())
				{
					RangoDistancia actual=iter.next();
					System.out.println("limite inferior: "+actual.getLimineInferior()+ " y limite superior: "+actual.getLimiteSuperior()+" realizo estos servicios: ");
					Iterator<Servicio> iter2=actual.getServiciosEnRango().iterator();
					while(iter2.hasNext())
					{
						Servicio actual2=iter2.next();
						System.out.println("En el servicio con id: "+actual2.getTripId()+" se recorrieron: "+actual2.getTripMiles()+ " millas desde las "+actual2.getFecha().getHoraInicio()+" hasta las "+actual2.getFecha().getHoraFinal()+" entre las fechas: "+actual2.getFecha().getFechaInicial()+" - "+actual2.getFecha().getFechaFinal());

					}
					System.out.println("--------------------------------------------------------");
				}	

				break;

			case 6: //1B
				ListaDoblementeEncadenada<Compania> lista=Controller.darCompaniasTaxisInscritos();
				//TODO
				//Mostrar la informacion de acuerdo al enunciado
				System.out.println("El total de compa��as con al menos un taxi inscrito es: "+lista.size());
				int contador=0;
				Iterator<Compania> iter1=lista.iterator();
				while(iter1.hasNext())
				{
					contador+=iter1.next().getTaxisInscritos().size();
				}
				System.out.println("El total de taxis afiliados a una compa��a es: "+contador);

				iter1=lista.iterator();
				while(iter1.hasNext())
				{
					Compania comp=iter1.next();
					System.out.println(comp.getNombre()+": "+comp.getTaxisInscritos().size()+" taxi(s)");
				}

				break;

			case 7: //2B
				try
				{
					//fecha inicial
					System.out.println("Ingrese la fecha inicial (Ej : 2017-02-01)");
					String fechaInicialReq2B = sc.next();

					//hora inicial
					System.out.println("Ingrese la hora inicial (Ej: 09:00:00.000)");
					String horaInicialReq2B = sc.next();

					//fecha final
					System.out.println("Ingrese la fecha final (Ej : 2017-02-01)");
					String fechaFinalReq2B = sc.next();

					//hora final
					System.out.println("Ingrese la hora final (Ej: 09:00:00.000)");
					String horaFinalReq2B = sc.next();

					//Compania
					System.out.println("Ingrese el nombre de la compa��a");

					String compania2B =sc.next()+" "+(sc.nextLine()).trim();


					if(fechaInicialReq2B.compareTo(fechaFinalReq2B)>=0)
					{	
						if(fechaInicialReq2B.compareTo(fechaFinalReq2B)==0&&horaInicialReq2B.compareTo(horaFinalReq2B)<=0)
						{

						}
						else
							throw new Exception("La fecha/hora inicial no puede ser depues de la fecha/hora final.");

					}


					//VORangoFechaHora
					RangoFechaHora rangoReq2B = new RangoFechaHora(fechaInicialReq2B, fechaFinalReq2B, horaInicialReq2B, horaFinalReq2B);

					Taxi taxi=Controller.darTaxiMayorFacturacion(rangoReq2B, compania2B);

					//TODO
					//Mostrar la informacion del taxi obtenido
					if(taxi==null)
						System.out.println("No hay ningun taxi de la compa�ia "+compania2B+" que haya genenerado servicios en el rango de tiempo dado");
					else
						System.out.println("El taxi que m�s ingresos genero en el rango de tiempo fue: "+taxi.getTaxiId());
				}
				catch (Exception e)
				{
					System.out.println(e.getMessage());
				}
				break;


			case 8: //3B
				//Compania
				System.out.println("Ingrese el id de la zona");
				String zona3B=sc.next();

				//fecha inicial
				System.out.println("Ingrese la fecha inicial (Ej : 2017-02-01)");
				String fechaInicialReq3B = sc.next();

				//hora inicial
				System.out.println("Ingrese la hora inicial (Ej: 09:00:00.000)");
				String horaInicialReq3B = sc.next();

				//fecha final
				System.out.println("Ingrese la fecha final (Ej : 2017-02-01)");
				String fechaFinalReq3B = sc.next();

				//hora final
				System.out.println("Ingrese la hora final (Ej: 09:00:00.000)");
				String horaFinalReq3B = sc.next();


				try
				{

					if(fechaInicialReq3B.compareTo(fechaFinalReq3B)>=0)
					{	
						if(fechaInicialReq3B.compareTo(fechaFinalReq3B)==0&&horaInicialReq3B.compareTo(horaFinalReq3B)<0)
						{

						}
						else
							throw new Exception("La fecha/hora inicial no puede ser depues de la fecha/hora final.");

					}


					RangoFechaHora rango3B= new RangoFechaHora(fechaInicialReq3B, fechaFinalReq3B, horaInicialReq3B, horaFinalReq3B);

					ServiciosValorPagado[] resp=Controller.darServiciosZonaValorTotal(rango3B, zona3B);
					//TODO
					//Mostrar la informacion de acuerdo al enunciado
					System.out.println("Se iniciaron "+resp[0].getServiciosAsociados().size()+" servicios en la zona "
							+zona3B+" que terminaron en otra zona.\n El valor total pagado fue de "+resp[0].getValorAcumulado());

					System.out.println("Se terminaron "+resp[1].getServiciosAsociados().size()+" servicios en la zona "
							+zona3B+" que iniciaron en otra zona.\n El valor total pagado fue de "+resp[1].getValorAcumulado());

					System.out.println("Se iniciaron y terminaron "+resp[2].getServiciosAsociados().size()+" servicios en la zona "
							+zona3B+"\n El valor total pagado fue de "+resp[2].getValorAcumulado());


				}
				catch (Exception e)
				{
					System.out.println(e.getMessage());
				}
				break;

			case 9: //4B
				//fecha inicial
				System.out.println("Ingrese la fecha inicial (Ej : 2017-02-01)");
				String fechaInicialReq4B = sc.next();

				//hora inicial
				System.out.println("Ingrese la hora inicial (Ej: 09:00:00.000)");
				String horaInicialReq4B = sc.next();

				//fecha final
				System.out.println("Ingrese la fecha final (Ej : 2017-02-01)");
				String fechaFinalReq4B = sc.next();

				//hora final
				System.out.println("Ingrese la hora final (Ej: 09:00:00.000)");
				String horaFinalReq4B = sc.next();

				try
				{

					if(fechaInicialReq4B.compareTo(fechaFinalReq4B)>=0)
					{	
						if(fechaInicialReq4B.compareTo(fechaFinalReq4B)==0&&horaInicialReq4B.compareTo(horaFinalReq4B)<=0)
						{

						}
						else
							throw new Exception("La fecha/hora inicial no puede ser depues de la fecha/hora final.");

					}

					RangoFechaHora rango4B= new RangoFechaHora(fechaInicialReq4B, fechaFinalReq4B, horaInicialReq4B, horaFinalReq4B);

					ListaDoblementeEncadenada<ZonaServicios> lista4B= Controller.darZonasServicios(rango4B);
					//TODO
					//Mostrar la informacion de acuerdo al enunciado

					Iterator<ZonaServicios> iter4b= lista4B.iterator();
					while(iter4b.hasNext())
					{
						ZonaServicios actual=iter4b.next();
						System.out.println("Zona: "+actual.getIdZona());
						Iterator<FechaServicios> iterFechas=actual.getFechasServicios().iterator();
						while(iterFechas.hasNext())
						{
							FechaServicios actFech=iterFechas.next();
							System.out.println("     En "+actFech.getFecha()+" hubo "+actFech.getNumServicios()+" servicios.");

						}
					}




				}
				catch (Exception e)
				{
					System.out.println(e.getMessage());
				}








				break;

			case 10: //2C
				System.out.println("Ingrese el n�mmero n de compa�ias");
				int n2C=sc.nextInt();

				//fecha inicial
				System.out.println("Ingrese la fecha inicial (Ej : 2017-02-01)");
				String fechaInicialReq2C = sc.next();

				//hora inicial
				System.out.println("Ingrese la hora inicial (Ej: 09:00:00.000)");
				String horaInicialReq2C = sc.next();

				//fecha final
				System.out.println("Ingrese la fecha final (Ej : 2017-02-01)");
				String fechaFinalReq2C = sc.next();

				//hora final
				System.out.println("Ingrese la hora final (Ej: 09:00:00.000)");
				String horaFinalReq2C = sc.next();
				try
				{
					if(fechaInicialReq2C.compareTo(fechaFinalReq2C)>=0)
					{	
						if(fechaInicialReq2C.compareTo(fechaFinalReq2C)==0&&horaInicialReq2C.compareTo(horaFinalReq2C)<=0)
						{

						}
						else
							throw new Exception("La fecha/hora inicial no puede ser depues de la fecha/hora final.");

					}
					if(n2C<0)
						throw new Exception("El n no puede ser negativo");

					RangoFechaHora rango2C= new RangoFechaHora(fechaInicialReq2C, fechaFinalReq2C, horaInicialReq2C, horaFinalReq2C);

					ILinkedList<Compania> lista2C= Controller.companiasMasServicios(rango2C, n2C);
					//TODO
					//Mostrar la informacion de acuerdo al enunciado	
					Iterator<Compania> iterador=lista2C.iterator();
					while(iterador.hasNext())
					{
						Compania actual=iterador.next();
						System.out.println(actual.getNombre()+": "+actual.getServiciosAsociados().size()+" servicios");
					}

				}
				catch (Exception e)
				{
					System.out.println(e.getMessage());
				}

				break;

			case 11: //3C
				ILinkedList<CompaniaTaxi> lista3C=Controller.taxisMasRentables();
				//TODO
				//Mostrar la informacion de acuerdo al enunciado				
				Iterator< CompaniaTaxi>iter3c=lista3C.iterator();
				while(iter3c.hasNext())
				{
					CompaniaTaxi actual=iter3c.next();
					double cantidad=actual.getTaxi().getDinero();
					if(actual.getTaxi().getMillas()==0)
					{
						cantidad=0;
					}
					else
					{
						cantidad/=actual.getTaxi().getMillas();
					}
					System.out.println("En la compania:"+actual.getNomCompania()+" el taxi mas rentable fue:"+ actual.getTaxi().getTaxiId()+ "con una propocion dinero/millas "+ cantidad);

				}

				break;

			case 12: //4C

				//id taxi
				System.out.println("Ingrese el id del taxi");
				String idTaxi4C=sc.next();

				//fecha 
				System.out.println("Ingrese la fecha inicial (Ej : 2017-02-01)");
				String fechaReq4C = sc.next();

				//hora inicial
				System.out.println("Ingrese la hora inicial (Ej: 09:00:00.000)");
				String horaInicialReq4C = sc.next();

				//hora final
				System.out.println("Ingrese la hora final (Ej: 09:00:00.000)");
				String horaFinalReq4C = sc.next();

				IStack <Servicio> resp4C=Controller.darServicioResumen(idTaxi4C, horaInicialReq4C, horaFinalReq4C, fechaReq4C);
				//TODO
				//Mostrar la informacion de acuerdo al enunciado
				Iterator<Servicio>iter4 = resp4C.iterator();
				while (iter4.hasNext()) 
				{
					Servicio actual=iter4.next();
					System.out.println("El id del servicio es: "+actual.getTripId()+"fue relizado por el taxi con id: "+actual.getTaxiId()+" que pertenece a la compania: "+actual.getCompania()+ " con hora inicial: "+actual.getFecha().getHoraInicio()+" y hora final: "+actual.getFecha().getHoraFinal()+" entre las fechas: "+actual.getFecha().getFechaInicial()+" - "+actual.getFecha().getFechaFinal());


				}

				break;

			case 13: //salir
				fin=true;
				sc.close();
				break;

			}
		}
	}
	/**
	 * Menu 
	 */
	private static void printMenu() //
	{
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Proyecto 1----------------------");
		System.out.println("Cargar data (1C):");
		System.out.println("1. Cargar toda la informacion dada una fuente de datos (small,medium, large).");

		System.out.println("\nParte A:\n");
		System.out.println("2.Obtenga una lista con todos los servicios de taxi ordenados cronologicamente por su fecha/hora inicial, \n"
				+ " que se prestaron en un periodo de tiempo dado por una fecha/hora inicial y una fecha/hora final de consulta. (1A) ");
		System.out.println("3.Dada una compania y un rango de fechas y horas, obtenga  el taxi que mas servicios inicio en dicho rango. (2A)");
		System.out.println("4.Consulte la compania, dinero total obtenido, servicios prestados, distancia recorrida y tiempo total de servicios de un taxi dados su id y un rango de fechas y horas. (3A)");
		System.out.println("5.Dada una fecha y un rango de horas, obtenga una lista de rangos de distancia en donde cada pocision contiene los servicios de taxis cuya distancia recorrida pertence al rango de distancia. (4A)\n");

		System.out.println("Parte B: \n");
		System.out.println("6. Obtenga el numero de companias que tienen al menos un taxi inscrito y el numero total de taxis que trabajan para al menos una compania. \n"
				+ "Luego, genera una lista (en orden alfabetico) de las companias a las que estan inscritos los servicios de taxi. Para cada una, indique su nombre y el numero de taxis que tiene registrados. (1B)");
		System.out.println("7. Dada una compania, una fecha/hora inicial y una fecha/hora final, buscar el taxi de la compania que mayor facturacion gener� en el tiempo dado. (2B)");
		System.out.println("8. Dada una zona de la ciudad, una fecha/hora inicial y una fecha/hora final, dar la siguiente informacion: (3B) \n"
				+ "   -Numero de servicios que iniciaron en la zona dada y terminaron en otra zona, junto con el valor total pagado por los usuarios. \n"
				+ "   -Numero de servicios que iniciaron en otra zona y terminaron en la zona dada, junto con el valor total pagado por los usuarios. \n"
				+ "   -Numero de servicios que iniciaron y terminaron en la zona dada, junto con el valor total pagado por los usuarios.");
		System.out.println("9. Dado un rango de fechas, obtener la lista de todas las zonas, ordenadas por su identificador. Para cada zona, dar la lista de fechas dentro del rango (ordenadas cronol�gicamente) \n "
				+ "y para cada fecha, dar el numero de servicios que se realizaron en dicha fecha. (4B)");


		System.out.println("\nParte C: \n");
		System.out.println("10. Dado un numero n, una fecha/hora inicial y una fecha/hora final, mostrar las n companias que mas servicios iniciaron dentro del rango. La lista debe estar ordenada descendentemente \n por el numero de servicios. Para cada compania, dar el nombre y el numero de servicios (2C)");
		System.out.println("11. Para cada compania, dar el taxi mas rentable. La rentabilidad es dada por la relacion entre el dinero ganado y la distancia recorrida. (3C)");
		System.out.println("12. Dado un taxi, dar el servicio resumen, resultado de haber comprimido su informacion segun el enunciado. (4C) ");
		System.out.println("13. Salir");
		System.out.println("Type the option number for the task, then press enter: (e.g., 1):");

	}

	private static void printMenuCargar()
	{
		System.out.println("-- Que fuente de datos desea cargar?");
		System.out.println("-- 1. Small");
		System.out.println("-- 2. Medium");
		System.out.println("-- 3. Large");
		System.out.println("-- Type the option number for the task, then press enter: (e.g., 1)");
	}

}
